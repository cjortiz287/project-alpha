from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


def show_project(request, id):
    projects = get_object_or_404(Project, id=id)
    return render(
        request, "projects/details_projects.html", {"projects": projects}
    )


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    return render(request, "projects/create_projects.html", {"form": form})
